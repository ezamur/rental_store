insert into customer (id, credit, points, username) values (1, 1000, 0, 'ezamur');
insert into customer (id, credit, points, username) values (2, 75, 0, 'dzamurovic');
insert into customer (id, credit, points, username) values (3, 200, 0, 'dusan');

insert into filmcategory (id, name, price) values (1, 'New release', 40);
insert into filmcategory (id, name, price) values (2, 'Regular release', 30);
insert into filmcategory (id, name, price) values (3, 'Old release', 30);

insert into film (id, name, category) values (1, 'Matrix 11', 1);
insert into film (id, name, category) values (2, 'Spiderman ', 2);
insert into film (id, name, category) values (3, 'Spiderman 2', 2);
insert into film (id, name, category) values (4, 'Out of Africa', 3);