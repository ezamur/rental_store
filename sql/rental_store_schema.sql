DROP TABLE customer;
CREATE TABLE customer (id BIGINT NOT NULL, credit DOUBLE PRECISION NOT NULL, points INTEGER NOT NULL, username CHARACTER VARYING(255) NOT NULL, PRIMARY KEY (id), CONSTRAINT uk_mufchskagt7e1w4ksmt9lum5l UNIQUE (username));
DROP TABLE film;
CREATE TABLE film (id BIGINT NOT NULL, name CHARACTER VARYING(255) NOT NULL, category INTEGER NOT NULL, PRIMARY KEY (id), CONSTRAINT uk_pa9n0l90b4yp8jo2o9x4onjcr UNIQUE (name));
DROP TABLE filmcategory;
CREATE TABLE filmcategory (id INTEGER NOT NULL, name CHARACTER VARYING(255) NOT NULL, price DOUBLE PRECISION NOT NULL, PRIMARY KEY (id), CONSTRAINT uk_l9018jm4yg7tl19a048fiduru UNIQUE (name));
DROP TABLE rentalrecord;
CREATE TABLE rentalrecord (id BIGINT NOT NULL, creationdate DATE NOT NULL, dayspaidupfront INTEGER NOT NULL, price DOUBLE PRECISION NOT NULL, returned BOOLEAN NOT NULL, customer_id BIGINT NOT NULL, film_id BIGINT NOT NULL, PRIMARY KEY (id));
ALTER TABLE film ADD CONSTRAINT fk_ndefmx6qscdwpnlde2mqm4y2t FOREIGN KEY (category) REFERENCES filmcategory (id);
ALTER TABLE rentalrecord ADD CONSTRAINT fk_bqd6i71j9ihiuqlvmdf6c4qve FOREIGN KEY (customer_id) REFERENCES customer (id);
ALTER TABLE rentalrecord ADD CONSTRAINT fk_inqy8qkq4dp7wfmeobav6ub1l FOREIGN KEY (film_id) REFERENCES film (id);
