package rs.ezamur.rental.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import rs.ezamur.rental.entity.Film;
import rs.ezamur.rental.repository.FilmRepository;
import rs.ezamur.rental.service.dto.FilmDTO;
import rs.ezamur.rental.service.dto.mapper.FilmObjectMapper;

@Service
public class FilmService {

    @Autowired
    protected FilmRepository filmRepository;

    @Autowired
    protected FilmObjectMapper filmObjectMapper;

    @Value("${price.premium}")
    private double premiumPrice;

    @Value("${price.basic}")
    private double basicPrice;

    @Value("${release.regular.fixed.price.period}")
    private int regularReleaseFixedPricePeriod;

    @Value("${release.old.fixed.price.period}")
    private int oldReleaseFixedPricePeriod;

    public double calculateRentingPrice(FilmDTO aFilm, int numberOfDays) {
        double price = .0;
        if (1L == aFilm.getCategoryId()) {
            // new release, predefined ID value
            price = calculateNewReleasePrice(numberOfDays);
        } else {
            // regular or old film, predefined ID value
            int fixedPricePeriod = regularReleaseFixedPricePeriod;
            if (3L == aFilm.getCategoryId()) {
                fixedPricePeriod = oldReleaseFixedPricePeriod;
            }
            price = calculateRegularOrOldReleasePrice(numberOfDays, fixedPricePeriod);
        }
        return price;
    }

    protected double calculateNewReleasePrice(int numberOfDays) {
        return premiumPrice * numberOfDays;
    }

    protected double calculateRegularOrOldReleasePrice(int numberOfDays, int fixedPricePeriod) {
        double price = .0;
        if (numberOfDays <= fixedPricePeriod) {
            price = basicPrice;
        } else {
            price = basicPrice + (numberOfDays - fixedPricePeriod) * basicPrice;
        }
        return price;
    }

    public int calculatePointsAwarded(FilmDTO f) {
        return f.getCategoryId() == 1 ? 2 : 1;
    }

    public List<FilmDTO> findAll() {
        List<Film> filmEntities = filmRepository.findAll();
        if (filmEntities == null || filmEntities.isEmpty()) {
            return Collections.emptyList();
        }

        List<FilmDTO> dtos = new ArrayList<FilmDTO>(filmEntities.size());
        for (Film f : filmEntities) {
            dtos.add(filmObjectMapper.toDto(f));
        }
        return dtos;
    }

    public FilmDTO findOne(long id) {
        return filmObjectMapper.toDto(filmRepository.findOne(id));
    }

}
