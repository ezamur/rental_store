package rs.ezamur.rental.service.dto;

import java.util.ArrayList;
import java.util.List;

import rs.ezamur.rental.service.dto.mapper.DTO;

public class RentalInvoiceDTO implements DTO {

    private List<FilmDTO> films;
    private CustomerDTO customer;
    private double totalPrice;
    private int pointsAwarded;

    public RentalInvoiceDTO() {
        super();
        films = new ArrayList<FilmDTO>(0);
        customer = null;
        totalPrice = .0;
        pointsAwarded = 0;
    }

    public List<FilmDTO> getFilms() {
        return films;
    }

    public void setFilms(List<FilmDTO> films) {
        this.films = films;
    }

    public CustomerDTO getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDTO customer) {
        this.customer = customer;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getPointsAwarded() {
        return pointsAwarded;
    }

    public void setPointsAwarded(int pointsAwarded) {
        this.pointsAwarded = pointsAwarded;
    }

}
