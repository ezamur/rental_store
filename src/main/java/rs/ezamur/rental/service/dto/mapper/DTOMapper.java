package rs.ezamur.rental.service.dto.mapper;

import rs.ezamur.rental.entity.Entity;

public interface DTOMapper<P extends Entity, V extends DTO> {

    V toDto(P entity);

}
