package rs.ezamur.rental.service.dto;

import rs.ezamur.rental.service.dto.mapper.DTO;

public class RentalDTO implements DTO {

    private long filmId;
    private int numberOfDays;

    public long getFilmId() {
        return filmId;
    }

    public void setFilmId(long filmId) {
        this.filmId = filmId;
    }

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

}
