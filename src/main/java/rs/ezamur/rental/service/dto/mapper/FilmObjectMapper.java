package rs.ezamur.rental.service.dto.mapper;

import org.springframework.stereotype.Component;

import rs.ezamur.rental.entity.Film;
import rs.ezamur.rental.service.dto.FilmDTO;

@Component
public class FilmObjectMapper implements DTOMapper<Film, FilmDTO> {

    @Override
    public FilmDTO toDto(Film entity) {
        FilmDTO dto = new FilmDTO();

        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setCategoryName(entity.getCategory().getName());
        dto.setCategoryId(entity.getCategory().getId());
        dto.setPrice(entity.getCategory().getPrice());

        return dto;
    }

}
