package rs.ezamur.rental.service.dto;

import rs.ezamur.rental.service.dto.mapper.DTO;

public class CustomerDTO implements DTO {

    private String username;
    private double credit;
    private int points;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

}
