package rs.ezamur.rental.service.dto.mapper;

import org.springframework.stereotype.Component;

import rs.ezamur.rental.entity.Customer;
import rs.ezamur.rental.service.dto.CustomerDTO;

@Component
public class CustomerObjectMapper implements DTOMapper<Customer, CustomerDTO> {

    @Override
    public CustomerDTO toDto(Customer entity) {
        CustomerDTO dto = new CustomerDTO();

        dto.setUsername(entity.getUsername());
        dto.setCredit(entity.getCredit());
        dto.setPoints(entity.getPoints());

        return dto;
    }

}
