package rs.ezamur.rental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import rs.ezamur.rental.entity.Customer;
import rs.ezamur.rental.repository.CustomerRepository;
import rs.ezamur.rental.service.dto.CustomerDTO;
import rs.ezamur.rental.service.dto.mapper.CustomerObjectMapper;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private CustomerObjectMapper dtoObjectMapper;

    public CustomerDTO findByUsername(String username) {
        Customer customer = customerRepository.findByUsername(username);
        if (customer == null) {
            throw new IllegalStateException("Customer with username " + username + " not found!");
        }

        return dtoObjectMapper.toDto(customer);
    }
}
