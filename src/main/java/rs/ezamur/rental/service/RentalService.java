package rs.ezamur.rental.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import rs.ezamur.rental.entity.Customer;
import rs.ezamur.rental.entity.Film;
import rs.ezamur.rental.entity.RentalRecord;
import rs.ezamur.rental.repository.CustomerRepository;
import rs.ezamur.rental.repository.FilmRepository;
import rs.ezamur.rental.repository.RentalRecordRepository;
import rs.ezamur.rental.service.dto.FilmDTO;
import rs.ezamur.rental.service.dto.RentalDTO;
import rs.ezamur.rental.service.dto.RentalInvoiceDTO;
import rs.ezamur.rental.service.dto.mapper.CustomerObjectMapper;
import rs.ezamur.rental.service.dto.mapper.FilmObjectMapper;

@Service
public class RentalService {

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private FilmService filmService;

    @Autowired
    private FilmRepository filmRepository;

    @Autowired
    protected FilmObjectMapper filmObjectMapper;

    @Autowired
    private RentalRecordRepository rentalRecordRepository;

    @Autowired
    private CustomerObjectMapper customerObjectMapper;

    @Transactional
    public RentalInvoiceDTO doRent(String username, List<RentalDTO> rentalDTOs) {
        Customer customer = customerRepository.findByUsername(username);
        if (customer == null) {
            throw new IllegalStateException("Customer with username " + username + " not found.");
        }

        RentalInvoiceDTO invoiceDTO = new RentalInvoiceDTO();

        double totalPrice = .0;
        int points = 0;
        List<RentalRecord> rentalRecords = new ArrayList<RentalRecord>(rentalDTOs.size());
        for (RentalDTO rentalDto : rentalDTOs) {
            FilmDTO f = filmService.findOne(rentalDto.getFilmId());
            double filmPrice = filmService.calculateRentingPrice(f, rentalDto.getNumberOfDays());
            f.setPrice(filmPrice);
            invoiceDTO.getFilms().add(f);

            totalPrice += filmPrice;
            points += filmService.calculatePointsAwarded(f);

            RentalRecord rentalRecord = new RentalRecord();
            rentalRecord.setCustomer(customer);
            rentalRecord.setCreationDate(new Date());
            rentalRecord.setFilm(filmRepository.findOne(f.getId()));
            rentalRecord.setPrice(filmPrice);
            rentalRecord.setDaysPaidUpfront(rentalDto.getNumberOfDays());
            rentalRecords.add(rentalRecord);
        }

        if (totalPrice > customer.getCredit()) {
            throw new IllegalStateException("Insufficient credit amount on your account.");
        }

        rentalRecordRepository.save(rentalRecords);

        invoiceDTO.setTotalPrice(totalPrice);
        invoiceDTO.setPointsAwarded(points);

        customer.setCredit(customer.getCredit() - totalPrice);
        customer.setPoints(customer.getPoints() + points);
        customer = customerRepository.save(customer);
        invoiceDTO.setCustomer(customerObjectMapper.toDto(customer));

        return invoiceDTO;
    }

    @Transactional
    public RentalInvoiceDTO doReturn(String username, List<RentalDTO> rentalDTOs) {
        Customer customer = customerRepository.findByUsername(username);
        if (customer == null) {
            throw new IllegalStateException("Customer with username " + username + " not found.");
        }

        RentalInvoiceDTO invoiceDTO = new RentalInvoiceDTO();

        double surcharges = .0;
        Date today = new Date();
        List<RentalRecord> rentalRecords = new ArrayList<RentalRecord>(rentalDTOs.size());
        for (RentalDTO rentalDto : rentalDTOs) {
            Film film = filmRepository.findOne(rentalDto.getFilmId());
            FilmDTO filmDTO = filmObjectMapper.toDto(film);

            RentalRecord rentalRecord = rentalRecordRepository.findByFilmAndCustomerAndReturnedFalse(film, customer);
            if (rentalRecord != null) {
                int dayDifference = calculateDateDiff(rentalRecord.getCreationDate(), today);
                if (dayDifference > rentalRecord.getDaysPaidUpfront()) {
                    double filmPrice = dayDifference * filmDTO.getPrice();
                    filmDTO.setPrice(filmPrice);
                    invoiceDTO.getFilms().add(filmDTO);

                    surcharges += filmPrice;
                }

                rentalRecord.setReturned(true);
                rentalRecords.add(rentalRecord);
            }
        }
        rentalRecordRepository.save(rentalRecords);

        invoiceDTO.setTotalPrice(surcharges);
        invoiceDTO.setPointsAwarded(0);

        customer.setCredit(customer.getCredit() - surcharges);
        customer = customerRepository.save(customer);
        invoiceDTO.setCustomer(customerObjectMapper.toDto(customer));

        return invoiceDTO;
    }

    private int calculateDateDiff(Date d1, Date d2) {
        long diffInMillis = d2.getTime() - d1.getTime();
        return (int) (diffInMillis / (24 * 60 * 60 * 1000));
    }

}
