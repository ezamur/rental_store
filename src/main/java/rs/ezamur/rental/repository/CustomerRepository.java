package rs.ezamur.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rs.ezamur.rental.entity.Customer;

@Repository
@Transactional
public interface CustomerRepository extends JpaRepository<Customer, Long> {

    Customer findByUsername(String username);

}
