package rs.ezamur.rental.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import rs.ezamur.rental.entity.Customer;
import rs.ezamur.rental.entity.Film;
import rs.ezamur.rental.entity.RentalRecord;

@Repository
public interface RentalRecordRepository extends JpaRepository<RentalRecord, Long> {

    RentalRecord findByFilmAndCustomerAndReturnedFalse(Film film, Customer customer);

}
