package rs.ezamur.rental.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import rs.ezamur.rental.entity.Film;

@Transactional(readOnly = true)
@Repository
public interface FilmRepository extends JpaRepository<Film, Long> {

    public List<Film> findByName(String name);

}
