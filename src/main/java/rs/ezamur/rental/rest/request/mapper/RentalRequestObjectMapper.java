package rs.ezamur.rental.rest.request.mapper;

import org.springframework.stereotype.Component;

import rs.ezamur.rental.rest.request.RentalRequest;
import rs.ezamur.rental.rest.response.mapper.RequestObjectMapper;
import rs.ezamur.rental.service.dto.RentalDTO;

@Component
public class RentalRequestObjectMapper implements RequestObjectMapper<RentalRequest, RentalDTO> {

    @Override
    public RentalDTO toDTO(RentalRequest request) {
        RentalDTO dto = new RentalDTO();

        dto.setFilmId(request.getFilmId());
        dto.setNumberOfDays(request.getNumberOfDays());

        return dto;
    }
}
