package rs.ezamur.rental.rest.response.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import rs.ezamur.rental.rest.response.RentalResponse;
import rs.ezamur.rental.service.dto.RentalInvoiceDTO;

@Component
public class RentalResponseObjectMapper implements ResponseObjectMapper<RentalInvoiceDTO, RentalResponse> {

    @Autowired
    private CustomerResponseObjectMapper customerResponseObjectMapper;

    @Autowired
    private FilmResponseObjectMapper filmResponseObjectMapper;

    @Override
    public RentalResponse toResponse(RentalInvoiceDTO dto) {
        RentalResponse response = new RentalResponse();

        response.setCustomer(customerResponseObjectMapper.toResponse(dto.getCustomer()));
        response.setFilms(filmResponseObjectMapper.toResponseList(dto.getFilms()));
        response.setPointsAwarded(dto.getPointsAwarded());
        response.setTotalPrice(dto.getTotalPrice());

        return response;
    }

}
