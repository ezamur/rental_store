package rs.ezamur.rental.rest.response.mapper;

import rs.ezamur.rental.rest.request.Request;
import rs.ezamur.rental.service.dto.mapper.DTO;

public interface RequestObjectMapper<P extends Request, V extends DTO> {

    V toDTO(P request);

}
