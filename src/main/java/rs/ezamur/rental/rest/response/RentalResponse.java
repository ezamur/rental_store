package rs.ezamur.rental.rest.response;

import java.util.ArrayList;
import java.util.List;

public class RentalResponse implements Response {

    private List<FilmResponse> films;
    private double totalPrice;
    private int pointsAwarded;
    private CustomerResponse customer;

    public RentalResponse() {
        films = new ArrayList<FilmResponse>(0);
        totalPrice = .0;
        pointsAwarded = 0;
        customer = null;
    }

    public List<FilmResponse> getFilms() {
        return films;
    }

    public void setFilms(List<FilmResponse> films) {
        this.films = films;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getPointsAwarded() {
        return pointsAwarded;
    }

    public void setPointsAwarded(int pointsAwarded) {
        this.pointsAwarded = pointsAwarded;
    }

    public CustomerResponse getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerResponse customer) {
        this.customer = customer;
    }

}
