package rs.ezamur.rental.rest.response.mapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Component;

import rs.ezamur.rental.rest.response.FilmResponse;
import rs.ezamur.rental.service.dto.FilmDTO;

@Component
public class FilmResponseObjectMapper implements ResponseObjectMapper<FilmDTO, FilmResponse> {

    @Override
    public FilmResponse toResponse(FilmDTO f) {
        FilmResponse film = new FilmResponse();

        film.setId(f.getId());
        film.setName(f.getName());
        film.setCategoryName(f.getCategoryName());
        film.setCategoryId(f.getCategoryId());
        film.setPrice(f.getPrice());

        return film;
    }

    public List<FilmResponse> toResponseList(List<FilmDTO> films) {
        List<FilmResponse> result = Collections.emptyList();

        if (films != null && !films.isEmpty()) {
            result = new ArrayList<FilmResponse>(films.size());
            for (FilmDTO f : films) {
                result.add(toResponse(f));
            }
        }

        return result;
    }

}
