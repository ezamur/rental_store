package rs.ezamur.rental.rest.response.mapper;

import org.springframework.stereotype.Component;

import rs.ezamur.rental.rest.response.CustomerResponse;
import rs.ezamur.rental.service.dto.CustomerDTO;

@Component
public class CustomerResponseObjectMapper implements ResponseObjectMapper<CustomerDTO, CustomerResponse> {

    @Override
    public CustomerResponse toResponse(CustomerDTO c) {
        CustomerResponse customer = new CustomerResponse();

        customer.setUsername(c.getUsername());
        customer.setCredit(c.getCredit());
        customer.setPoints(c.getPoints());

        return customer;
    }

}
