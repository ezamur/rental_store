package rs.ezamur.rental.rest.response.mapper;

import rs.ezamur.rental.rest.response.Response;
import rs.ezamur.rental.service.dto.mapper.DTO;

public interface ResponseObjectMapper<P extends DTO, V extends Response> {

    V toResponse(P dto);

}
