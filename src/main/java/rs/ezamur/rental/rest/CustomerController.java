package rs.ezamur.rental.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ezamur.rental.rest.request.RentalRequest;
import rs.ezamur.rental.rest.request.mapper.RentalRequestObjectMapper;
import rs.ezamur.rental.rest.response.CustomerResponse;
import rs.ezamur.rental.rest.response.RentalResponse;
import rs.ezamur.rental.rest.response.mapper.CustomerResponseObjectMapper;
import rs.ezamur.rental.rest.response.mapper.RentalResponseObjectMapper;
import rs.ezamur.rental.service.CustomerService;
import rs.ezamur.rental.service.RentalService;
import rs.ezamur.rental.service.dto.CustomerDTO;
import rs.ezamur.rental.service.dto.RentalDTO;
import rs.ezamur.rental.service.dto.RentalInvoiceDTO;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private RentalService rentalService;

    @Autowired
    private CustomerResponseObjectMapper customerObjectMapper;

    @Autowired
    private RentalRequestObjectMapper rentalRequestObjectMapper;

    @Autowired
    private RentalResponseObjectMapper rentalResponseObjectMapper;

    @RequestMapping(value = "{username}", method = RequestMethod.GET)
    public CustomerResponse getCustomerByUsername(@PathVariable("username") String username) {
        CustomerDTO customerDto = customerService.findByUsername(username);
        if (customerDto != null) {
            return customerObjectMapper.toResponse(customerDto);
        } else {
            return null;
        }
    }

    @RequestMapping(value = "rent/{username}", method = RequestMethod.POST)
    public RentalResponse doRent(@PathVariable("username") String username,
            @RequestBody List<RentalRequest> rentRequests) {
        List<RentalDTO> rentalDTOs = preProcess(rentRequests);

        RentalInvoiceDTO invoiceDTO = rentalService.doRent(username, rentalDTOs);
        return rentalResponseObjectMapper.toResponse(invoiceDTO);
    }

    @RequestMapping(value = "return/{username}", method = RequestMethod.POST)
    public RentalResponse doReturn(@PathVariable("username") String username,
            @RequestBody List<RentalRequest> rentRequests) {
        List<RentalDTO> rentalDTOs = preProcess(rentRequests);

        RentalInvoiceDTO invoiceDTO = rentalService.doReturn(username, rentalDTOs);
        return rentalResponseObjectMapper.toResponse(invoiceDTO);
    }

    private List<RentalDTO> preProcess(List<RentalRequest> rentRequests) {
        if (rentRequests == null || rentRequests.isEmpty()) {
            throw new IllegalStateException("No film found in rent request.");
        }

        List<RentalDTO> rentalDTOs = new ArrayList<RentalDTO>(rentRequests.size());
        for (RentalRequest r : rentRequests) {
            rentalDTOs.add(rentalRequestObjectMapper.toDTO(r));
        }

        return rentalDTOs;
    }

}
