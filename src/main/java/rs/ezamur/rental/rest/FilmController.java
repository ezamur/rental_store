package rs.ezamur.rental.rest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import rs.ezamur.rental.rest.response.FilmResponse;
import rs.ezamur.rental.rest.response.mapper.FilmResponseObjectMapper;
import rs.ezamur.rental.service.FilmService;
import rs.ezamur.rental.service.dto.FilmDTO;

@RestController
@RequestMapping("/film")
public class FilmController {

    @Autowired
    protected FilmService filmService;

    @Autowired
    protected FilmResponseObjectMapper mapper;

    @RequestMapping(value = "films", method = RequestMethod.GET)
    public List<FilmResponse> listFilms() {
        List<FilmDTO> filmEntities = filmService.findAll();
        if (filmEntities == null || filmEntities.isEmpty()) {
            return new ArrayList<FilmResponse>(0);
        }
        return mapper.toResponseList(filmEntities);
    }

}