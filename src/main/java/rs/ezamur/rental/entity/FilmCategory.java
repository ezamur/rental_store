package rs.ezamur.rental.entity;

import javax.persistence.Column;

/**
 * Film category entity. Allows flexible categorization of films by persisting categories in database.
 *
 */
@javax.persistence.Entity
public class FilmCategory extends Entity<Integer> {

    private static final long serialVersionUID = -7370531265198427638L;

    @Column(unique = true, nullable = false)
    private String name;

    @Column(nullable = false)
    private double price;

    public FilmCategory() {
        super();
        name = "";
        price = .0;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

}
