package rs.ezamur.rental.entity;

import javax.persistence.Column;
import javax.persistence.ManyToOne;

/**
 * Film entity.
 *
 */
@javax.persistence.Entity
public class Film extends Entity<Long> {

    private static final long serialVersionUID = -4814835746417871267L;

    @Column(unique = true, nullable = false)
    private String name;

    @ManyToOne(optional = false)
    private FilmCategory category;

    public Film() {
        super();
        name = "";
        category = null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FilmCategory getCategory() {
        return category;
    }

    public void setCategory(FilmCategory category) {
        this.category = category;
    }

}
