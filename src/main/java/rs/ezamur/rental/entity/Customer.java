package rs.ezamur.rental.entity;

import javax.persistence.Column;

/**
 * Customer entity.
 *
 */
@javax.persistence.Entity
public class Customer extends Entity<Long> {

    private static final long serialVersionUID = -1136747571676346471L;

    @Column(unique = true, nullable = false)
    private String username;

    @Column(nullable = false)
    private double credit;

    @Column(nullable = false)
    private int points;

    public Customer() {
        super();
        username = "";
        credit = .0;
        points = 0;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getCredit() {
        return credit;
    }

    public void setCredit(double credit) {
        this.credit = credit;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

}
