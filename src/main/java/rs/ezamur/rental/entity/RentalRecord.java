package rs.ezamur.rental.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entity representing a single rental record of a {@link Customer}.
 */
@javax.persistence.Entity
public class RentalRecord extends Entity<Long> {

    private static final long serialVersionUID = -726304045965925100L;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false)
    protected Date creationDate;

    @ManyToOne
    @JoinColumn(name = "customer_id", nullable = false)
    private Customer customer;

    @ManyToOne
    @JoinColumn(name = "film_id", nullable = false)
    private Film film;

    @Column(nullable = false)
    private double price;

    @Column(nullable = false)
    private int daysPaidUpfront;

    @Column(nullable = false)
    private boolean returned;

    public RentalRecord() {
        super();
        creationDate = new Date();
        customer = new Customer();
        film = new Film();
        price = .0;
        daysPaidUpfront = 0;
        returned = false;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDaysPaidUpfront() {
        return daysPaidUpfront;
    }

    public void setDaysPaidUpfront(int daysPaidUpfront) {
        this.daysPaidUpfront = daysPaidUpfront;
    }

    public boolean isReturned() {
        return returned;
    }

    public void setReturned(boolean returned) {
        this.returned = returned;
    }

}
