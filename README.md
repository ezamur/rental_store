# VIDEO RENTAL STORE #

## How to run the app ##

### Requirements ###

* Gradle 2.3
* PostgreSQL 9.3.6

### Setup ###

* Create rental_store database using PostgreSQL tools (in my case username/pass combination was admin/admin)
```
#!shell

createdb -U admin rental_store
Password:
```

* Get the code

```
#!shell

$ git clone https://ezamur@bitbucket.org/ezamur/rental_store.git
$ cd rental_store/

```

* Initialize database structure and insert data

Find src/main/resources/application.properties file and update two properties like given bellow:

```
#!shell

...
spring.jpa.generate-ddl=true
spring.jpa.hibernate.ddl-auto=update
...
```

After the first run, those can and should be switched back to initial values (false and none, respectively).

To insert initial set of data, use given rental_store_data.sql file.

```
#!shell

$ psql -U admin -d rental_store -a -f sql/rental_store_data.sql
$ Password:
```

* run the app

```
#!shell

$ gradle bootRun
...
INFO rs.ezamur.rental.AppConfig - Started AppConfig in 4.649 seconds (JVM running for 5.028) [org.springframework.boot.StartupInfoLogger logStarted 56]
```

## The API ##

* list of films in inventory

This endpoint can be used by customers to browse film inventory.

```
#!

GET http://localhost:8080/rental-store/film/films
```

Response example:

```
#!
[{"id":1,"name":"Matrix 11","categoryName":"New release","categoryId":1,"price":40.0},{"id":2,"name":"Spiderman ","categoryName":"Regular release","categoryId":2,"price":30.0},{"id":3,"name":"Spiderman 2","categoryName":"Regular release","categoryId":2,"price":30.0},{"id":4,"name":"Out of Africa","categoryName":"Old release","categoryId":3,"price":30.0}]
```


* customer profile

This endpoint can be used by customer to check his current credit amount and the value of points earned so far.


```
#!

GET http://localhost:8080/rental-store/customer/ezamur
```

Response example:

```
#!

{"username":"ezamur","credit":1000.0,"points":0}
```


* renting of the films

This endpoint is used by the customer to specify which movies he/she wants to rend and for how long


```
#!

POST http://localhost:8080/rental-store/customer/rent/ezamur
headers Content-Type: application/json

json request body example: [{"filmId": 1, "numberOfDays": 1}, {"filmId": 2, "numberOfDays": 5}, {"filmId": 3, "numberOfDays": 2}, {"filmId": 4, "numberOfDays": 7}]
```

Response example:

```
#!
{"films":[{"id":1,"name":"Matrix 11","categoryName":"New release","categoryId":1,"price":40.0},{"id":2,"name":"Spiderman ","categoryName":"Regular release","categoryId":2,"price":90.0},{"id":3,"name":"Spiderman 2","categoryName":"Regular release","categoryId":2,"price":30.0},{"id":4,"name":"Out of Africa","categoryName":"Old release","categoryId":3,"price":90.0}],"totalPrice":250.0,"pointsAwarded":5,"customer":{"username":"ezamur","credit":750.0,"points":5}}
```

Insufficient credit example:

```
#!

{"timestamp":1455026314220,"status":500,"error":"Internal Server Error","exception":"java.lang.IllegalStateException","message":"Insufficient credit amount on your account.","path":"/rental-store/customer/rent/dusan"}
```


* returning the films


```
#!

POST http://localhost:8080/rental-store/customer/return/ezamur
headers Content-Type: application/json

json request body example: [{"filmId": 1}, {"filmId": 2}]
```

Response example (in case of late return):

```
#!

{"films":[{"id":1,"name":"Matrix 11","categoryName":"New release","categoryId":1,"price":80.0}],"totalPrice":80.0,"pointsAwarded":0,"customer":{"username":"ezamur","credit":670.0,"points":5}}
```

## Discussion ##

I focused on delivering specified requirements with just small addition of few API calls that I found reasonable to have in this very first draft version of the app. There are still many places for improvements:

* there is no security system included into this example - in case we decided to have it, Spring security would be a nice way to go as I already used Spring Boot
* categories of the films could be modelled differently - not to hold the price information, as I did it for the sake of shorter developmen, but to keep the price information separated from categories and thus, make more flexible and configurable
* information about customers should be more extensive - attributes like email address for communication with a customer, tracking the things customer searches for, recommending according to the customer's previous searches and similar
* API for searching the titles can be extended massively - searching by name, by genre, by length, by actors in the movie, by topic, by similarity with other films, etc...
* this kind of website could be easily turned into some kind of social network so maybe even relationships between customers make some sense
* providing logging in with Google+ profile, Facebook, Twitter, LinkedIn account... In general, grabbing some opportunity to connect with social networks...

* no tests

There are no tests included. This is very bad but while I was working on the code, I realized there are no many places where business logic is complex and really demands testing.
Repositories are generated by Spring framework so, again, no sense in testing them.
Testing service classes would require mocking repositories which I would do with a lib like Mockito, for example. Same stands for controllers.
Object mappers are pretty much straight forward implementations and I decided, again for the sake of time saving, not to do it.

* customer credit

I wasn't sure how to implement payment methods so I decided to go with the shortest variant - an assumption that customer has some kind of pre-paid credit and that video rental store can withdraw money from his account. Current implementation allows that customer goes into "minus" after surcharges which I don't like. But, if we imagine that rental store is really able to withdraw money from customer's credit card, for example, then this would not be an issue.
We would have to take care that customer doesn't remove his payment methods while he has some films rented, i.e. before he/she returns them and portal checks for possible surcharges.

* reason behind choosing PostreSQL

I had it installed on my laptop.

* reason behind choosing Spring Boot

I had some previous experience with it while I haven't played with mentioned dropwizard.


## Other tools used ##

I was using Advanced REST client Google Chrome extension for firing requests and testing the API.